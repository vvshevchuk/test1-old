<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Lesson;
use Illuminate\Http\Request;

class MainController extends Controller
{
  public function index()
  {
    $categories = Category::orderBy('title')->get();
    $lessons = Lesson::paginate(4);
    return view('main', [
      'lessons' => $lessons,
      'categories'=> $categories,
      ]);
  }
}
