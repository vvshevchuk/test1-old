<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function getLessonsByCategory($slug)
    {
        $categories = Category::orderBy('title')->get();
        $currentCategory = Category::where('slug',$slug)->first();
        return view('main',[
            'lessons' => $currentCategory->lessons()->paginate(4),
            'categories'=>$categories,
        ]);
    }

    public function getLesson($slugCategory,$slugLesson)
    {
        $categories = Category::orderBy('title')->get();
        $lesson = Lesson::where('slug',$slugLesson)->first();
        return view('show-lesson',[
            'lesson' => $lesson,
            'categories' => $categories,
        ]);
    }
}
