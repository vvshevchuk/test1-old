<div>
    @foreach($categories as $category)
        <a style="background-color: #0c81e4; color: black" href="{{route('getLessonsByCategory',$category->slug)}}">{{$category->title}}</a>
    @endforeach
</div>
