@extends('main-layout')

@section('main_content')
    <div style="padding-top: 100px">
        @include('categories')
        <div>
            <a href="{{route('getLessonsByCategory',$lesson->category->slug)}}" class="btn btn-outline-primary mb-4">Back</a>
        </div>
        <h1>{{$lesson->title}}</h1>
        <div>
            {!! $lesson->text !!}
        </div>
    </div>
@endsection
